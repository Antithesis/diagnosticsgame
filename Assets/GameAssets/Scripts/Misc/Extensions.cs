﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class Extensions {

    public static void SafeInvoke(this UnityEvent unityEvent) {
        if(unityEvent != null)
        {
            unityEvent.Invoke();
        }
    }

    public static void SafeInvoke<T>(this UnityEvent<T> unityEvent, T arg0)
    {
        if (unityEvent != null)
        {
            unityEvent.Invoke(arg0);
        }
    }

    public static T GetRandom<T>(this List<T> list)
    {
        T result;

        if (list.Count == 0)
        {
            return default(T);
        }

        if (list.Count == 1)
        {
            return list[0];
        }

        else
        {
            return result = list[Random.Range(0, list.Count - 1)];
        }



    }



}
