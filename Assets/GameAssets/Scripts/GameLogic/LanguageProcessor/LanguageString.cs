﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageString : List<string>
{
    public string languageString;
    public LanguageMachine.StringType Type;
    public LanguageMachine.Article Article;
    public List<string> Tag;

    public LanguageString(string languageString, LanguageMachine.StringType type, LanguageMachine.Article article = LanguageMachine.Article.NONE)
    {
        this.languageString = languageString;
        this.Type = type;
        this.Article = article;
    }

    public LanguageString addTag(string tag)
    {
        this.Tag.Add(tag);
        return this;
    }


}
