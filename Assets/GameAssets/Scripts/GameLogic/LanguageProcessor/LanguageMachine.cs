﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageMachine
{
    public Patient Patient;
    private Symptom symptom;

    public static char[] vowels = new char[]
    {
        'a', 'e', 'i', 'o', 'u', 'y'
    };

    public static char[] consonants = new char[]
    {
        'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y'
    };

    private static readonly List<string> introText = new List<string>
    {
        "I have conducted a patient interview with %patient_name%.",
        "%patient_name% came to the office today.",
        "%patient_name% seemed quite bewildered when %third_person% reached out to me today."
    };

    private static readonly List<string> introduction = new List<string>
    {
        "%patient_short% came to me because %third_person%",
        "%patient_short%",
    };

    private static readonly List<string> connectors = new List<string>
    {
        "It was also revealed that %patient_short%",
        "and",
        "In addition, %patient_short%",
        "not to mention that %patient_short%",
        "It's also worth noting"
    };

    public static readonly List<string> ConnectorWordHas = new List<string>
    {
        "has"
    };

    public static readonly List<string> ConnectorWordFeels = new List<string>
    {
        "feels",
        "experiences feelings of",
        "has a sensation of"
    };

    public static readonly List<string> ConnectorWordIs = new List<string>
    {
        "is"
    };



    public enum Article
    {
        A,
        AN,
        NONE
    }

    public enum StringType
    {
        INTRO,
        HAS,
        IS,
        FEELS,
    }

    public enum NodeType
    {
        INTRO,
        FIRSTPHRASE,
        PERSON,
        SYMPTOM,
        CONNECTOR
    }

    public List<LanguageNode> LanguageNodes = new List<LanguageNode>();

    public LanguageNode StartNode;

    public string GenerateString(List<LanguageString> languageString)
    {
        LanguageNode intro = new LanguageNode(new LanguageString(introText.GetRandom(), StringType.INTRO), this);
        LanguageNodes.Add(intro);

        LanguageNode firstPhrase = new LanguageNode(new LanguageString(introduction.GetRandom(), StringType.INTRO), this);
        LanguageNodes.Add(firstPhrase);

        foreach (LanguageString LS in languageString)
        {

            string connector = connectors.GetRandom();
            LanguageNode connectorNode = new LanguageNode(new LanguageString(connector, StringType.FEELS), this);

            LanguageNodes.Add(connectorNode);

            LanguageNode prefix = null;

            if (LS.Type == StringType.FEELS)
            {
                prefix =new LanguageNode(new LanguageString(ConnectorWordFeels.GetRandom(), StringType.FEELS), this);
            }

            else if (LS.Type == StringType.HAS)
            {
                prefix = new LanguageNode(new LanguageString(ConnectorWordHas.GetRandom(), StringType.HAS), this);
            }

            else if (LS.Type == StringType.IS)
            {
               prefix = new LanguageNode(new LanguageString(ConnectorWordIs.GetRandom(), StringType.IS), this);
            }

            if (prefix != null)
            {
                LanguageNodes.Add(prefix);
            }

            string article = "";

            if (LS.Article == Article.A)
            {
                article = "a";
            }

            else if (LS.Article == Article.AN)
            {
                article = "an";
            }

            if (article != "")
            {
                LanguageNode articleNode = new LanguageNode(new LanguageString(article, StringType.IS), this);
                LanguageNodes.Add(articleNode);
            }

            LanguageNode node = new LanguageNode(LS, this);
            LanguageNodes.Add(node);
        }

        string text = "";
        foreach (LanguageNode node in LanguageNodes)
        {
            text += node.getText();
        }

        return text;
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
