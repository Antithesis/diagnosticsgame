﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageNode
{
    private LanguageMachine Machine;
    public LanguageString text;
    public LanguageMachine.StringType Type;
    public LanguageMachine.Article Article;
    public bool endWithPunctuation;

    public LanguageNode(LanguageString text, LanguageMachine machine)
    {
        this.Machine = machine;
        this.text = text;
    }

    public string getText()
    {
        if (endWithPunctuation)
        {
            return string.Format(" {0}.", text.languageString);
        }

        return string.Format(" {0} ", text.languageString);

    }


}
