﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Constraints;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;


public class PatientUpdatedEvent : UnityEvent<Patient> { }

public class GameController : GameSingleton<GameController>
{
    [OdinSerialize]
    public PatientUpdatedEvent PatientUpdatedEvent;

    public void RegisterNewPatient()
    {
        Patient p = PatientGenerator.Instance.GeneratePatient();
        PatientUpdatedEvent.SafeInvoke(p);     
    }

    void Awake()
    {
        PatientUpdatedEvent = new PatientUpdatedEvent();
    }
	// Use this for initialization
	void Start () {
        RegisterNewPatient();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
