﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

public class GameTimer : SerializedMonoBehaviour
{

    [OdinSerialize]
    public int TimeNow { get; set; }
    public List<ScheduledEvent> Events { get; set; }


	// Use this for initialization
	void Start () {
        Events = new List<ScheduledEvent>();		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class ScheduledEvent
{
    protected UnityAction Action;
    protected int FireAtTime;


}