﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextCreator : GameSingleton<TextCreator>
{


    public string ReplaceTags(string inputString, Patient p)
    {
        string returnString = inputString;

        returnString = returnString.Replace(PATIENT_NAME_TOKEN, string.Format("{0} {1}", p.FirstName, p.LastName));

        if (p.Age > 30)
        {
            returnString = returnString.Replace(PATIENT_SHORT_TOKEN, string.Format("{0}", GetFormalName(p)));
        }

        else
        {
            returnString = returnString.Replace(PATIENT_SHORT_TOKEN, string.Format("{0}", GetPersonalName(p)));
        }

        returnString = returnString.Replace(THIRD_PERSON_TOKEN, getThirdPerson(p));
        returnString = returnString.Replace(THIRD_PERSON_POSSESSIVE_TOKEN, getPossessive(p));
        returnString = returnString.Replace(THIRD_PERSON_OBJECT_TOKEN, getThirdPersonObject(p));

        return returnString;
    }

    private static readonly string PATIENT_NAME_TOKEN = "%patient_name%";
    private static readonly string THIRD_PERSON_TOKEN = "%third_person%";
    private static readonly string THIRD_PERSON_POSSESSIVE_TOKEN= "%third_person_posess%";
    private static readonly string THIRD_PERSON_OBJECT_TOKEN = "%third_person_object%";

    private static readonly string PATIENT_SHORT_TOKEN = "%patient_short%";



    private static readonly string THIRD_PERSON_MALE = "he";
    private static readonly string THIRD_PERSON_FEMALE = "she";

    private static readonly string THIRD_PERSON_MALE_POSSESSIVE = "his";
    private static readonly string THIRD_PERSON_FEMALE_POSESSIVE = "her";

    private static readonly string THIRD_PERSON_MALE_OBJECT = "him";
    private static readonly string THIRD_PERSON_FEMALE_OBJECT = "her";

    public string getPossessive(Patient p)
    {
        if (p.PatientSex == Patient.Sex.M)
        {
            return THIRD_PERSON_MALE_POSSESSIVE;
        }

        else
        {
            return THIRD_PERSON_FEMALE_POSESSIVE;
        }
    }

    public string getThirdPerson(Patient p)
    {
        if (p.PatientSex == Patient.Sex.M)
        {
            return THIRD_PERSON_MALE;
        }

        else
        {
            return THIRD_PERSON_FEMALE;
        }
    }

    public string getThirdPersonObject(Patient p)
    {
        if (p.PatientSex == Patient.Sex.M)
        {
            return THIRD_PERSON_MALE_OBJECT;
        }

        else
        {
            return THIRD_PERSON_FEMALE_OBJECT;
        }
    }

    public string GetFormalName(Patient p)
    {
        if (p.PatientSex == Patient.Sex.M)
        {
            return string.Format("Mr. {0}", p.LastName);
        }

        else
        {
            int married = Random.Range(0, 100);

            if (married > 50 && p.Age > 20)
            {
                return string.Format("Mrs. {0}", p.LastName);
            }

            else
            {
                return string.Format("Miss {0}", p.LastName);
            }
        }


    }

    public string GetPersonalName(Patient p)
    {
        return p.FirstName;
    }


    //public string getIntroText()
    //{
    //    return introText[Random.Range(0, introText.Count - 1)];
    //}

    //public string GetInitialPhrase()
    //{
    //    return introduction[Random.Range(0, introText.Count - 1)];
    //}

    //public string GetConnector()
    //{
    //    return connectors[Random.Range(0, introText.Count - 1)];
    //}


    public string GenerateSickHistory(Patient p)
    {
        LanguageMachine machine = new LanguageMachine();

        List<LanguageString> languageString = new List<LanguageString>();

        foreach (SymptomEntry entry in p.SymptomEntry)
        {
            languageString.Add(entry.Entry);
        }

        return ReplaceTags(machine.GenerateString(languageString), p);
        //string fullText = "";

        //fullText += getIntroText();

        //int max = p.SymptomEntry.Count;

        //for (int i = 0; i < p.SymptomEntry.Count; i++)
        //{
        //    if (i == 0)
        //    {
        //        fullText += GetInitialPhrase();
        //    }

        //    else
        //    {
        //        fullText += GetConnector();
        //    }

        //    fullText += string.Format("{0} ", p.SymptomEntry[i].Entry);
        //}

        //{
        //};

        //return ReplaceTags(fullText, p);
    }

}
