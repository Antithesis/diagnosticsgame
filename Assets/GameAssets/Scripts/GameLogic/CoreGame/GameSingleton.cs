﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSingleton<T> : MonoBehaviour where T : MonoBehaviour {

    private static T instance;

    public static T Instance
    {
        get
        {
            if(instance != null)
            {
                return instance;
            }

            T component = GameObject.FindObjectOfType<T>();
            if(component != null)
            {
                instance = component.GetComponent<T>();
            }

            else
            {
                GameObject gameObject= new GameObject();
                instance = gameObject.AddComponent<T>();
                gameObject.name = instance.name;
            }

            return instance;
        }

        private set
        {
            instance = value;
        }

    }

}
