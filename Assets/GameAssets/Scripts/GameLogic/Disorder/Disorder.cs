﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public abstract class Disorder : SerializedMonoBehaviour
{

    public enum Severity
    {
        MILD,
        MODERATE,
        SEVERE,
        LIFE_THREATENING
    }

    [OdinSerialize]
    public Severity DisorderSeverity { get; set; }

    [OdinSerialize]
    public bool TargetDisorder { get; set; }

    public Patient ParentPatient { get; set; }

    [OdinSerialize]
    public bool AlreadyKnown { get; set; }

    public virtual void Apply(Patient p)
    {
        p.Disorders.Add(this);
    }

    [OdinSerialize]
    public List<Syndrom> Syndroms { get; set; }

    public virtual void OnTestPerformed(string id)
    {

    }

    void Awake()
    {
        Syndroms = new List<Syndrom>();
    }

    // Use this for initialization
    void Start ()
    {
        this.gameObject.name = this.GetType().Name;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public List<SymptomEntry> GetSymptomEntries()
    {
        List<SymptomEntry> symptomEntry = new List<SymptomEntry>();

        foreach (Syndrom syndrom in Syndroms)
        {
            foreach (SymptomEntry entry in syndrom.GetSymptomEntries())
            {
                symptomEntry.Add(entry);
            }

        }
        return symptomEntry;
    }







}
