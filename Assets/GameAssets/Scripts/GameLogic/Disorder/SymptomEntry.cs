﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SymptomEntry
{
    public Symptom Symptom;
    public LanguageString Entry;
    public Text RelatedUIComponent;

    public SymptomEntry(Symptom symptom, LanguageString entry)
    {
        this.Symptom = symptom;
        this.Entry = entry;
    }


}
