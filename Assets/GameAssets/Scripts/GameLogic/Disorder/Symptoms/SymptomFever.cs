﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;


public class SymptomFever : Symptom {

    public enum FeverStrength {MILD, MODERATE, HIGH, LETHAL}

    public FeverStrength Strength;
    private float fever;

    public override Result onPerformTest(Test test, List<Result> results)
    {
        throw new System.NotImplementedException();
    }

    public override void Apply(Syndrom s)
    {
        base.Apply(s);
        ParentSyndrom = s;
    }

    public override void GenerateSymptomEntry()
    {
        this.HistoryDescription = new List<LanguageString>();

        switch (Strength)
        {
            case FeverStrength.MILD:
                fever = Random.Range(37.5f, 38.4f);
                HistoryDescription.Add(new LanguageString("slightly shivery and cold", LanguageMachine.StringType.FEELS));
                break;
            case FeverStrength.MODERATE:
                fever = Random.Range(38.5f, 39.9f);
                HistoryDescription.Add(new LanguageString("poorly and weak", LanguageMachine.StringType.FEELS));
                break;
            case FeverStrength.HIGH:
                fever = Random.Range(40f, 41.9f);
                HistoryDescription.Add(new LanguageString("very poorly and shivers visibly", LanguageMachine.StringType.IS));
                break;
            case FeverStrength.LETHAL:
                fever = Random.Range(42f, 43.9f);
                HistoryDescription.Add(new LanguageString("drowzy and unresponsive", LanguageMachine.StringType.IS));
                ParentSyndrom.ParentDisorder.ParentPatient.Tags.Add("ill");
                break;
        }

        base.GenerateSymptomEntry();

    }


    public override void Construct()
    {

    }
}
