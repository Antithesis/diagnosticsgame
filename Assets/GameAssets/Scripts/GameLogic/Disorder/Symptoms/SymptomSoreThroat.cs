﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;


public class SymptomSoreThroat : Symptom {

    public enum Strength {MILD, MODERATE, HIGH}

    public Strength SorenessStrength;

    public override Result onPerformTest(Test test, List<Result> results)
    {
        throw new System.NotImplementedException();
    }

    public override void Apply(Syndrom s)
    {
        base.Apply(s);
        ParentSyndrom = s;
    }

    public override void GenerateSymptomEntry()
    {
        this.HistoryDescription = new List<LanguageString>();
        switch (SorenessStrength)
        {
            case Strength.MILD:
                HistoryDescription.Add(new LanguageString("slight discomfort in %third_person_posess% throat.", LanguageMachine.StringType.FEELS));
                break;
            case Strength.MODERATE:
                HistoryDescription.Add(new LanguageString("pain in %third_person_posess% throat and slight trouble swallowing.", LanguageMachine.StringType.HAS));
                break;
            case Strength.HIGH:
                HistoryDescription.Add(new LanguageString("severe pain in %third_person_posess% throat and difficulty swallowing.", LanguageMachine.StringType.FEELS));
                break;
        }

        base.GenerateSymptomEntry();

    }

    public override void Construct()
    {

    }
}
