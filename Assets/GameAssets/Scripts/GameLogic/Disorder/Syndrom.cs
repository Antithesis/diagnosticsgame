﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class Syndrom : System.Object
{

    public abstract Result onPerformTest(string test, List<Result> results);

    public Disorder ParentDisorder { get; set; }
    public List<Symptom> Symptoms = new List<Symptom>();

    public virtual void Apply(Disorder parentDisorder)
    {
        ParentDisorder = parentDisorder;
        parentDisorder.Syndroms.Add(this);
    }
    public bool Active { get; set; }

    public virtual void OnTestPerformed(string id)
    {

    }

    public List<SymptomEntry> GetSymptomEntries()
    {
        List<SymptomEntry> symptomEntry = new List<SymptomEntry>();

        foreach (Symptom symptom in Symptoms)
        {
            if (symptom.Active && symptom.History)
            {
                symptomEntry.Add(symptom.SymptomEntry);
            }
        
        }

        return symptomEntry;
    }

}
