﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cold : Disorder {

	// Use this for initialization
    public override void Apply(Patient p)
    {
        base.Apply(p);
        new SyndromCold().Apply(this);
    }

}
