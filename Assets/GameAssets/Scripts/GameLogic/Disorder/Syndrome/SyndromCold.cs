﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyndromCold : Syndrom {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override Result onPerformTest(string test, List<Result> results)
    {
        throw new System.NotImplementedException();
    }

    public override void Apply(Disorder d)
    {
        base.Apply(d);
        SymptomFever fever = new SymptomFever();
        fever.Strength = SymptomFever.FeverStrength.MILD;
        fever.History = true;
        fever.Active = true;
        fever.Apply(this);

        SymptomSoreThroat throat = new SymptomSoreThroat();
        throat.SorenessStrength = SymptomSoreThroat.Strength.MILD;
        throat.History = true;
        throat.Active = true;
        throat.Apply(this);

    }
}
