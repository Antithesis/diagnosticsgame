﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class DisorderGenerator : GameSingleton<DisorderGenerator>
{

    public class DisorderIDRarity
    {
        public string id;
        public int rarity;

        public DisorderIDRarity(string id, int rarity)
        {
            this.id = id;
            this.rarity = rarity;
        }
    }



    public readonly List<DisorderIDRarity> Disorders = new List<DisorderIDRarity>
    {
        new DisorderIDRarity("DisorderCold", 0)
    };


    public Disorder GenerateDisorder(int rarity = 0)
    {
        List<DisorderIDRarity> disorders = Disorders.FindAll(x => x.rarity >= rarity);
        string disorder;

        if (disorders.Count == 0)
        {
            return null;
        }

        else if (disorders.Count == 1)
        {
            disorder = disorders[0].id;
        }

        else
        {
            disorder = disorders[Random.Range(0, disorders.Count - 1)].id;
        }

        return GenerateDisorder(disorder);

    }

    Disorder GenerateDisorder(string id)
    {

        GameObject go = new GameObject();
        Disorder disorder = null;

        switch (id)
        {
            case "DisorderCold":
                disorder = go.AddComponent<Cold>();
                break;
        }

        return disorder;
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
