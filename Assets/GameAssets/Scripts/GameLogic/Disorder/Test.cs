﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class Test : SerializedMonoBehaviour
{
    [OdinSerialize]
    public string testID { get; set; }
    [OdinSerialize]
    public string TestName { get; set; }
    [OdinSerialize]
    public string TestDescription { get; set; }

    [OdinSerialize]
    public bool LocationSpecific { get; set; }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
