﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public abstract class Symptom: System.Object
{

    public abstract Result onPerformTest(Test test, List<Result> results);

    public virtual void Apply(Syndrom s)
    {
        this.ParentSyndrom = s;
        this.ParentSyndrom.Symptoms.Add(this);
    }

    public Syndrom ParentSyndrom { get; set; }

    [OdinSerialize]
    public bool Active { get; set; }


    [OdinSerialize]
    private bool history;
    public bool History
    {
        get { return history; }
        set
        {
            if (value)
            {
                GenerateSymptomEntry();
            }
            history = value;
        } }

    public List<LanguageString> HistoryDescription { get; set; }


    public List<Patient.BodyPart> PossibleBodyParts;
    public List<Bodypart> AffectedBodyParts;
    public abstract void Construct();

    public SymptomEntry SymptomEntry { get; set; }

    public virtual void GenerateSymptomEntry()
    {
        LanguageString entry = HistoryDescription[Random.Range(0, HistoryDescription.Count)];
        SymptomEntry = new SymptomEntry(this, entry);
    }

}
