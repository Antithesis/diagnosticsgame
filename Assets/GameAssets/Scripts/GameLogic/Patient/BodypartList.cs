﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

public class BodypartList
{
    public List<Bodypart> BodyParts;

    private static BodypartList globalBodypartList;

    private static Bodypart Get(Patient.BodyPart bodyPart)
    {
        return GlobalBodyPartsList.BodyParts.Find(x => x.BodyPart == bodyPart).Copy();
    }

    public static BodypartList GlobalBodyPartsList
    {
        get
        {
            if (globalBodypartList == null)
            {
                globalBodypartList = new BodypartList();
            }

            return globalBodypartList;
        }
        private set { globalBodypartList = value; }
    }

    //public enum BodyPart
    //{ HEAD, EARS, EYES, NOSE, MOUTH, ORAL_MUCOSA, ORAL_TONGUE, LYMPH_NODES, NECK, CHEST, BREASTS, LUNGS, HEART, ARMS, STOMACH, LIVER, APPENDIX, BLADDER, UTERUS, LEGS, MUSCLES, JOINTS }

    // Use this for initialization
    public BodypartList()
    {
        this.BodyParts = new List<Bodypart>()
        {
            new Bodypart(Patient.BodyPart.APPENDIX, "Appendix", false),
            new Bodypart(Patient.BodyPart.ARMS, "Arms", false),
            new Bodypart(Patient.BodyPart.BLADDER, "Bladder", false),
            new Bodypart(Patient.BodyPart.BREASTS, "Breasts", false),
            new Bodypart(Patient.BodyPart.CHEST, "Chest", false),
            new Bodypart(Patient.BodyPart.EARS, "Ears", false),
            new Bodypart(Patient.BodyPart.EYES, "Eyes", false),
            new Bodypart(Patient.BodyPart.HEAD, "Head", false),
            new Bodypart(Patient.BodyPart.HEART, "Heart", false),
            new Bodypart(Patient.BodyPart.JOINTS, "Joints", false),
            new Bodypart(Patient.BodyPart.LEGS, "Legs", false),
            new Bodypart(Patient.BodyPart.LIVER, "Liver", false),
            new Bodypart(Patient.BodyPart.LUNGS, "Lungs", false),
            new Bodypart(Patient.BodyPart.CHEST, "Chest", false),
            new Bodypart(Patient.BodyPart.UTERUS, "Uterus", false)
        };
    }

}
