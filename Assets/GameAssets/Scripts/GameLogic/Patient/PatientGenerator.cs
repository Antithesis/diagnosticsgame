﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientGenerator : GameSingleton<PatientGenerator>
{
    private readonly List<string> firstNamesMale = new List<string>
    {
        "Daniel", "Bob", "Christian", "Rosensteel", "Magewanti", "Hip-Hopalinen"
    };

    private readonly List<string> firstNamesFemale = new List<string>
    {
        "Cristina", "Christina", "Angelita", "Muriel", "Anne-Lise", "Julie", "Trembelina", "Blubberina"

    };

    private readonly List<string> lastNames = new List<string>
    {
        "Hammer", "Baker", "Miller", "Swilt", "Copper"
    };

    public Patient GeneratePatient()
    {
        GameObject go = new GameObject();
        Patient patient = go.AddComponent<Patient>();
        patient.FirstName = firstNamesMale[Random.Range(0, firstNamesMale.Count)];
        patient.LastName = lastNames[Random.Range(0, lastNames.Count)];
        patient.Age = Random.Range(16, 70);
        patient.Height = Random.Range(145, 210);

        float sexProbability = Random.Range(1, 100);
        if (sexProbability > 50)
        {
            patient.PatientSex = Patient.Sex.M;
        }

        else
        {
            patient.PatientSex = Patient.Sex.F;
        }

        DisorderGenerator.Instance.GenerateDisorder().Apply(patient);


        patient.GenerateSickHistory();
        return patient;
    }




}
