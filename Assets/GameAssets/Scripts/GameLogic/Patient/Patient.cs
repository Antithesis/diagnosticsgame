﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;


public class Patient : SerializedMonoBehaviour
{

    public enum Sex
    { M, F}

    public enum BodyPart
    { HEAD, EARS, EYES, NOSE, MOUTH, ORAL_MUCOSA, ORAL_TONGUE, LYMPH_NODES, NECK, CHEST, BREASTS, LUNGS, HEART, ARMS, STOMACH, LIVER, APPENDIX, BLADDER, UTERUS, LEGS, MUSCLES, JOINTS}

    [SerializeField]
    private string firstName;
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    [SerializeField]
    private string lastName;
    public string LastName
    {
        get { return lastName;}
        set { lastName = value; }
    }

    [SerializeField]
    private int age;

    public int Age
    {
        get { return age;}
        set { age = value; }
    }

    private Sex patientSex;
    public Sex PatientSex
    {
        get
        {
            return patientSex;

        }

        set { patientSex = value; }
    }


    [SerializeField]
    private int height;
    public int Height
    {
        get { return height;}
        set { height = value; }
    }

    public List<Disorder> Disorders;

    [OdinSerialize]
    public List<string> Tags;

    [OdinSerialize]
    public List<SymptomEntry> SymptomEntry;

    public string Interview;

    public void GenerateSickHistory()
    {
        SymptomEntry = new List<SymptomEntry>();
        foreach (Disorder d in Disorders)
        {
            foreach (SymptomEntry entry in d.GetSymptomEntries())
            {
                SymptomEntry.Add(entry);                
            }
        }

        Interview = TextCreator.Instance.GenerateSickHistory(this);
    }


    void Awake()
    {
        Disorders = new List<Disorder>();
        
    }

	// Use this for initialization
	void Start () {

    }

    
	
	// Update is called once per frame
	void Update () {
		
	}
}
