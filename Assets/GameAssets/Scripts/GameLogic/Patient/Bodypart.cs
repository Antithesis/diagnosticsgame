﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.Serialization;
using UnityEngine;

public class Bodypart
{
    [OdinSerialize]
    public Patient.BodyPart BodyPart { get; set; }
    [OdinSerialize]
    public string BodyPartName { get; set; }
    [OdinSerialize]
    public bool Active { get; set; }

    public Bodypart(Patient.BodyPart bodyPart, string name, bool active)
    {
        this.BodyPart = bodyPart;
        this.BodyPartName = name;
        this.Active = active;
    }

    public Bodypart Copy()
    {
        Bodypart bodyPart = new Bodypart(this.BodyPart, this.BodyPartName, this.Active);
        return bodyPart;
    }

    

}

public class BodypartLikelihood
{
    [OdinSerialize]
    public Patient.BodyPart BodyPart { get; set; }
    [OdinSerialize]
    public int Likelihood { get; set; }
}
