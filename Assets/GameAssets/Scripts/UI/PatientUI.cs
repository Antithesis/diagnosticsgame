﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PatientUI : MonoBehaviour
{

    public Text Name;
    public Text Age;
    public Text Sex;
    public Text Height;
    public Text Interview;

	// Use this for initialization
	void Start () {
        		
	}

    void Awake()
    {
        GameController.Instance.PatientUpdatedEvent.AddListener(UpdatePatient);
    }
    
	// Update is called once per frame
	void Update () {
		
	}

    void UpdatePatient(Patient p)
    {
        Name.text = string.Format("{0} {1}", p.FirstName, p.LastName);
        Age.text = p.Age.ToString();
        Sex.text = p.PatientSex == Patient.Sex.M ? Sex.text = "Male" : Sex.text = "Female";
        Height.text = string.Format("{0} {1}", p.Height.ToString(), "cm");
        Interview.text = p.Interview;

    }

}
